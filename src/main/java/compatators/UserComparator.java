package compatators;

import models.BrandUser;
import org.apache.hadoop.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 24/10/12
 * Time: 6:02 PM
 */
public class UserComparator extends WritableComparator {

    public UserComparator(){
        super(BrandUser.class, true);
    }

    protected UserComparator(Class<? extends WritableComparable> keyClass) {
        super(BrandUser.class,true);
    }

    @Override
    public int compare(Object a, Object b) {
        return ((BrandUser)a).compareTo(((BrandUser) b));
    }
}