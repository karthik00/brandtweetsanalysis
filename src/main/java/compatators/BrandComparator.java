package compatators;

import models.BrandUser;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 25/10/12
 * Time: 3:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrandComparator extends WritableComparator {

    public BrandComparator(){
        super(BrandUser.class, true);
    }

    protected BrandComparator(Class<? extends WritableComparable> keyClass) {
        super(BrandUser.class, true);
    }

    @Override
    public int compare(Object a, Object b) {
        return ((BrandUser)a).brand_name.compareTo(((BrandUser) b).brand_name);
    }
}
