package mapreds;

import models.BrandUser;
import models.Tweet;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 23/10/12
 * Time: 6:20 PM
 */
public class TweetsMapper extends MapReduceBase implements Mapper<LongWritable, Tweet, BrandUser, Tweet> {

    public void map(LongWritable longWritable, Tweet tweet, OutputCollector<BrandUser, Tweet> brandTweetOutputCollector, Reporter reporter) throws IOException {
        BrandUser buser = new BrandUser();
        buser.user_name=tweet.getUser_name();
        buser.brand_name=tweet.getBrand();
        brandTweetOutputCollector.collect(buser, tweet);
        if(!initialInsertsMade(reporter, tweet.getBrand())){
            BrandUser endFlag = new BrandUser();
            endFlag.user_name="*";
            endFlag.brand_name=buser.brand_name;
            brandTweetOutputCollector.collect(endFlag, tweet);
            reporter.getCounter(buser.brand_name,buser.brand_name).increment(1);
        }
    }

    HashMap<String, Boolean> brand_inits_set=new HashMap<String, Boolean>();

    private boolean initialInsertsMade(Reporter reporter, String brand_name){
        if(brand_inits_set.get(brand_name)!=null){
            if(!brand_inits_set.get(brand_name).booleanValue()){
                brand_inits_set.put(brand_name, new Boolean(reporter.getCounter(brand_name,brand_name).getValue()!=0));
            }
            return brand_inits_set.get(brand_name).booleanValue();
        }
        else {
           brand_inits_set.put(brand_name, new Boolean(reporter.getCounter(brand_name,brand_name).getValue()!=0));
           return brand_inits_set.get(brand_name).booleanValue();
        }
    }
}