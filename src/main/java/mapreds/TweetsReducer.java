package mapreds;

import models.BrandUser;
import models.Tweet;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 23/10/12
 * Time: 6:26 PM
 */
public class TweetsReducer extends MapReduceBase implements Reducer<BrandUser, Tweet, Text, Text> {

    HashMap<String, Integer> user_count_map=new HashMap<String, Integer>();
    HashMap<String, Integer> repeated_user_count_map=new HashMap<String, Integer>();
    HashMap<String, Integer> tweet_count_map=new HashMap<String, Integer>();

    public void reduce(BrandUser brand_user, Iterator<Tweet> tweetIterator, OutputCollector<Text, Text> textTextOutputCollector, Reporter reporter) throws IOException{
        if(brand_user.user_name.equals("*")){
            textTextOutputCollector.collect(new Text(brand_user.brand_name), new Text("Tweet count " + tweet_count_map.get(brand_user.brand_name).intValue()));
            textTextOutputCollector.collect(new Text(brand_user.brand_name), new Text("user count "+user_count_map.get(brand_user.brand_name).intValue()));
            textTextOutputCollector.collect(new Text(brand_user.brand_name), new Text("repeat user count "+repeated_user_count_map.get(brand_user.brand_name).intValue()));
            return;
        }
        int tweet_count=0;
        while(tweetIterator.hasNext()){
            tweet_count++;
            tweetIterator.next();
        }

        if(tweet_count>1){
            if(repeated_user_count_map.get(brand_user.brand_name)!=null){
                repeated_user_count_map.put(brand_user.brand_name, new Integer(repeated_user_count_map.get(brand_user.brand_name).intValue()+1));
            }
            else{
                repeated_user_count_map.put(brand_user.brand_name, new Integer(1));
            }
        }

        if(tweet_count_map.get(brand_user.brand_name)!=null){
            tweet_count_map.put(brand_user.brand_name, new Integer(tweet_count_map.get(brand_user.brand_name).intValue()+tweet_count));
        }
        else{
            tweet_count_map.put(brand_user.brand_name, new Integer(tweet_count));
        }

        if(user_count_map.get(brand_user.brand_name)!=null){
              user_count_map.put(brand_user.brand_name, new Integer(user_count_map.get(brand_user.brand_name).intValue()+1));
        }
        else{
              user_count_map.put(brand_user.brand_name, new Integer(1));
        }

    }
}