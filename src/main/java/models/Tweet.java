package models;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 22/10/12
 * Time: 5:25 PM
 */

public class Tweet implements DBWritable, WritableComparable<Tweet>{

    private String  id;
    private String  brand;
    private String  user_name;
    private String  creation_date;
    private String  text;
    private String  geo;
    private String  iso_language;

    public String getBrand() {
        return brand;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getText() {
        return text;
    }

    public void write(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, this.id);
        preparedStatement.setString(2, this.brand);
        preparedStatement.setString(3, this.user_name);
        preparedStatement.setString(5, this.text);
        preparedStatement.setString(6, this.geo);
        preparedStatement.setString(7, this.iso_language);
    }

    public void readFields(ResultSet resultSet) throws SQLException {
        this.id=resultSet.getString(1);
        this.brand=resultSet.getString(2);
        this.user_name=resultSet.getString(3);
        this.text=resultSet.getString(5).replaceAll("\n", " ");
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeBytes(toString()+"\n");
    }

    public void readFields(DataInput dataInput) throws IOException {
        try{
            String str=dataInput.readLine();
            //System.out.println(str);
            if(str!=null){
                String[] arr = str.split("#!#");
                this.id=arr[0];
                this.brand=arr[1];
                this.user_name=arr[2];
            }
        }catch (Exception ignore){
            System.out.println(ignore);
        }
    }

    public String toString(){
        return this.id+"#!#"+this.brand+"#!#"+this.user_name+"#!#"+this.creation_date+"#!#"+this.text+"#!#"+this.geo+"#!#"+this.iso_language;
    }

    public int compareTo(Tweet tweet) {
        return this.id.compareTo(tweet.id);
    }

    @Override
    public boolean equals(Object o) {
        return this.id.equals(((Tweet)o).id);
    }
}