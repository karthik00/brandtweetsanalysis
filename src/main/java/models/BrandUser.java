package models;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 25/10/12
 * Time: 4:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrandUser implements WritableComparable<BrandUser> {
    public String brand_name;
    public String user_name;

    public int compareTo(BrandUser brandUser) {
        if(brandUser.user_name.equals("*"))
            return -1;
        if(this.user_name.equals("*"))
            return 1;
        if(user_name.compareTo(brandUser.user_name)!=0)
            return user_name.compareTo(brandUser.user_name);
        else
            return brand_name.compareTo(brandUser.brand_name);
    }

    @Override
    public boolean equals(Object o) {
        return this.user_name.equals(((BrandUser)o).user_name) && this.brand_name.equals(((BrandUser)o).brand_name);
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeBytes(toString()+"\n");
    }

    public void readFields(DataInput dataInput) throws IOException {
        try{
            String str=dataInput.readLine();
            if(str!=null){
                String[] arr = str.split(",");
                this.user_name=arr[0];
                this.brand_name=arr[1];
            }
        }catch (Exception ignore){
            System.out.println(ignore);
        }
    }

    @Override
    public String toString() {
        return user_name+","+brand_name;
    }
}