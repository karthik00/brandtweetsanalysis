package outputformaters;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 24/10/12
 * Time: 11:59 AM
 */
public class BrandOutputFormat extends MultipleTextOutputFormat<Text, Text> {
    @Override
    protected String generateFileNameForKeyValue(Text key, Text value, String name) {
        return key.toString();
    }
}
