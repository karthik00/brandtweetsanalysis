import compatators.BrandComparator;
import compatators.UserComparator;
import mapreds.TweetsMapper;
import mapreds.TweetsReducer;
import models.BrandUser;
import models.Tweet;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.lib.db.DBConfiguration;
import org.apache.hadoop.mapred.lib.db.DBInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import outputformaters.BrandOutputFormat;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 23/10/12
 * Time: 5:42 PM
 */
public class MapReduceTool extends Configured implements Tool {
    public int run(String[] strings) throws Exception {
        Configuration conf=getConf();
        JobConf job = new JobConf(conf);
        job.setJobName("chainExe");
        job.setInputFormat(DBInputFormat.class);
        job.setOutputFormat(BrandOutputFormat.class);

        job.setOutputKeyClass(BrandUser.class);
        job.setOutputValueClass(Tweet.class);
        job.setOutputKeyComparatorClass(BrandComparator.class);
        job.setOutputValueGroupingComparator(UserComparator.class);

        job.setReducerClass(TweetsReducer.class);
        job.setOutputKeyComparatorClass(BrandComparator.class);
        job.setOutputValueGroupingComparator(UserComparator.class);

        job.setMapperClass(TweetsMapper.class);
        BrandOutputFormat.setOutputPath(job, new Path("/Users/karthik/Desktop/hadoop_data/tmp"));
        DBConfiguration.configureDB(job, "com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/twitter_data?user=root&password=123");
        String [] fields = { "id", "brand", "user_name", "creation_date", "text", "geo", "iso_language"};
        DBInputFormat.setInput(job, Tweet.class, "brand_tweets", null ,  "id", fields);

        job.setNumReduceTasks(4);
        job.setNumMapTasks(5);
        JobClient.runJob(job);
        return 0;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new MapReduceTool(), args);
        System.exit(res);
    }
}
